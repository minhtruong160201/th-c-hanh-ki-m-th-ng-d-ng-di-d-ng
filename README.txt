# THỰC HÀNH KIỂM THỬ ỨNG DỤNG DI ĐỘNG
=== Giới thiệu project ===
Project Thực hành kiểm thử ứng dụng di động là testcase để test ứng dụng đặt đồ uống online
=== Các thành phần trong project ===
- 1 file excel chứa testcase kiểm thử ứng dụng đặt đồ uống online
- 1 file excel chứa defect log các lỗi đã tìm ra
- 1 file apk của ứng dụng đặt đồ uống online
- 1 file word chức chứa tài liệu chức năng nghiệp vụ của ứng dụng đặt đồ uống online
- 1 file README tổng quan về project
=== Cách sử dụng ===
- Sử dụng file apk trong project để cài đặt ứng dụng đặt đồ uống online
- Sử dụng phần mềm excel, word để đọc tài liệu chức năng nghiệp vụ và testcase
- Sử dụng testcase để test ứng dụng đặt đồ uống online